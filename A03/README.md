# LIS3781 Advanced Database Management

## Alex Moerschbacher

### Assignment 3 Requirements:
    - Connect to an Oracle DBMS 
    - Create a database on Oracle DBMS
    - Learn differences in creating tables in storing data in an Oracle DBMS than others

## SQL Code
![First image of code](Code1.png)
![Second image of code](Code2.png)
![Third image of code](Code3.png)

## Populated Tables
![Sql Tables](table1.png)
![Sql Tables](table2.png)
![Sql Tables](table3.png)

## Link to Repos
- The link for [LIS3781 repo](https://bitbucket.org/amm16aa/lis3781/src/master/)