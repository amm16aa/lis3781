# LIS3781 Advanced Database Management

## Alex Moerschbacher

### Assignment 2 Requirements:
    - Install Ampps Locally
    - Create a bitbucket account
    - Create a database which uses multiple users accounts differ roles, permissions and access-levels

## SQL Code
![First image of code](Code1.png)
![Second image of code](Code2.png)
![Third image of code](Code3.png)

## Populated Tables
![Sql Tables](tables.png)

## Git Commands
- git init - Git init creates a new git repository.
- git status - Shows the status of the git repository. What is different between the local and 
remote repository.
- git add - Add file contents to the index which can then be added to the repository through git 
commit.
- git commit - Records changes to the repository
- git push - update remote repository to match local
- git pull - updates local repository to match remote
- git log - Shows all commit history  

## Link to Repos
- The link for [LIS3781 repo](https://bitbucket.org/amm16aa/lis3781/src/master/)
- The link for [BitbucketStationLocations repo](https://bitbucket.org/amm16aa/bitbucketstationlocations/src/master/)