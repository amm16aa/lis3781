# LIS3781 Advanced Database Management

## Alex Moerschbacher

### Project 2 Requirements:
    - Use MongoDB a NoSQL solution
    - Use a find statement 
    - Gain experience searching inside embedded documents

## NOSQL Code
![First image of code](code1.png)

## Populated Tables
![Sql Tables](table1.png)

## Link to Repos
- The link for [LIS3781 repo](https://bitbucket.org/amm16aa/lis3781/src/master/)