# LIS3781 Advanced Database Management

## Alex Moerschbacher

### Assignment 4 Requirements:
    - Connect to an MicrosoftSQL DBMS 
    - Create a database on MicrosoftSQL DBMS
    - Learn differences in creating tables in storing data in an MicrosoftSQL DBMS than others

## SQL Code
![First image of code](code1.png)
![Second image of code](code2.png)
![Third image of code](code3.png)
![Third image of code](code4.png)
![Third image of code](code5.png)
![Third image of code](code6.png)
![Third image of code](code7.png)
![Third image of code](code8.png)
![Third image of code](code9.png)
![Third image of code](code10.png)

## Populated Tables
![Sql Tables](table1.png)
![Sql Tables](table2.png)
![Sql Tables](table3.png)
![Sql Tables](table4.png)
![Sql Tables](table5.png)
![Sql Tables](table6.png)
![Sql Tables](table7.png)
![Sql Tables](table8.png)
![Sql Tables](table9.png)
![Sql Tables](table10.png)
![Sql Tables](table11.png)
![Sql Tables](table12.png)
![Sql Tables](table13.png)
![Sql Tables](table14.png)

## Link to Repos
- The link for [LIS3781 repo](https://bitbucket.org/amm16aa/lis3781/src/master/)