
# LIS3781 Advanced Database Management

## Alex Moerschbacher

### LIS3781 Requirements:
- Perform advanced relational data modeling, using Entity-Relationship diagrams.
- Design a data warehouse.
- Apply performance tuning techniques
- Administer and perform backup and recovery procedures
- Justify anad define user's roles, permissions, and access-levels as a database administrator.
- Create database security settings
- Demonstrate an understanding of data warehousing, data mining, and data analytic techniques
*Course Work Links:*

1. [A01 README.md](A01/README.md "My A1 README.md file")
    - Create a database which uses Time-Variant Data
    - Use Zero-fill data in a database

2. [A02 README.md](A02/README.md "My A2 README.md file")
    - Install Ampps Locally
    - Create a bitbucket account
    - Create a database which uses multiple users accounts differ roles, permissions and access-levels
3. [A03 README.md](A03/README.md "My A3 README.md file")
    - Connect to an Oracle DBMS 
    - Create a database on Oracle DBMS
    - Learn differences in creating tables in storing data in an Oracle DBMS than others
4. [P01 README.md](P01/README.md "My P01 README.md file")
    - Utilize Triggers in a Database
    - Create a View 
    - Create a stored procedure which populates data into tables for security purposes
5. [A04 README.md](A04/README.md "My A04 README.md file")
    - Connect to an MicrosoftSQL DBMS 
    - Create a database on MicrosoftSQL DBMS
    - Learn differences in creating tables in storing data in an MicrosoftSQL DBMS than others
6. [A05 README.md](A05/README.md "My A05 README.md file")
    - Connect to an MicrosoftSQL DBMS 
    - Create a database on MicrosoftSQL DBMS
    - Learn differences in creating tables in storing data in an MicrosoftSQL DBMS than others
    - Created a Stored Procedure
    - Create a Virtual Table using a Trigger### Assignment 5 Requirements:
    - Connect to an MicrosoftSQL DBMS 
    - Create a database on MicrosoftSQL DBMS
    - Learn differences in creating tables in storing data in an MicrosoftSQL DBMS than others
    - Created a Stored Procedure
    - Create a Virtual Table using a Trigger
7. [P02 README.md](P02/README.md "My P02 README.md file")
    - Use MongoDB a NoSQL solution
    - Use a find statement 
    - Gain experience searching inside embedded documents