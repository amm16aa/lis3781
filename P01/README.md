# LIS3781 Advanced Database Management

## Alex Moerschbacher

### Project 1 Requirements:
    - Utilize Triggers in a Database
    - Create a View 
    - Create a stored procedure which populates data into tables for security purposes

## SQL Code
![First image of code](code1.png)
![Second image of code](code2.png)
![Third image of code](code3.png)
![Fourth image of code](code4.png)
![Fifth image of code](code5.png)
![Fixth image of code](code6.png)
![Seventh image of code](code7.png)
![Eighth image of code](code8.png)
![Nineth image of code](code9.png)
![Tenth image of code](code10.png)

## Populated Tables
![Sql Tables](table1.png)
![Sql Tables](table2.png)
![Sql Tables](table3.png)
![Sql Tables](table4.png)
![Sql Tables](table5.png)
![Sql Tables](table6.png)
![Sql Tables](table7.png)
![Sql Tables](table8.png)
![Sql Tables](table9.png)
![Sql Tables](table10.png)
![Sql Tables](table11.png)

## Link to Repos
- The link for [LIS3781 repo](https://bitbucket.org/amm16aa/lis3781/src/master/)